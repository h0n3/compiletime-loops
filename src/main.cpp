#include <iostream>
#include <tuple>
#include <functional>

#include "ct_for_each.hpp"

// Actual function implementations
void functor1_function1(int data)
{
    std::cout << "functor1 function1 " << std::to_string(data) << std::endl;
}
void functor1_function2(double data)
{
    std::cout << "functor1 function2 " << std::to_string(data) << std::endl;
}

void functor2_function1(int data)
{
    std::cout << "functor2 function1 " << std::to_string(data) << std::endl;
}
void functor2_function2(double data)
{
    std::cout << "functor2 function2 " << std::to_string(data) << std::endl;
}

struct compile_time_interface {
    struct function1_resolver {
        template <class functor_t>
        static constexpr auto resolve(functor_t& functor)
        {
            return &functor_t::function1;
        }
    };

    template <class... functors_t>
    void call_function1(std::tuple<int>& data, std::tuple<functors_t...>& functors)
    {
        ct_for_each_tuple<function1_resolver, std::tuple<int>, functors_t...>(data, functors);
    }

    DEFINE_FUNCTION(function2, double);
};

template <class... functors_t>
class user_module {
    std::tuple<functors_t...> _functors;

public:
    void function_using_function1(int data)
    {
        auto data_wrapper = std::make_tuple(data);
        ct_for_each_tuple<compile_time_interface::function1_resolver>(data_wrapper, _functors);
    }

    void function_using_function2(double data)
    {
        auto data_wrapper = std::make_tuple(data);
        compile_time_interface::call_function2(data_wrapper, _functors);
    }
};

struct functor1 {
    void function1(int data)
    {
        functor1_function1(data);
    }
    void function2(double data)
    {
        functor1_function2(data);
    }
};

struct functor2 {
    void function1(int data)
    {
        functor2_function1(data);
    }
    void function2(double data)
    {
        functor2_function2(data);
    }
};

void impl_normal(int i, double d){
        std::cout << "Normal implementation" << std::endl;
        functor1_function1(i);
        functor2_function1(i);

        functor1_function2(d);
        functor2_function2(d);
}

void impl_ct_loop(int i, double d){
    std::cout << "Ct-loop implementation" << std::endl;
    user_module<functor1, functor2> module;

    module.function_using_function1(i);
    module.function_using_function2(d);
}


int main(void)
{
    int i{ 42 };
    double d{42.0};

    impl_normal(i,d);
    impl_ct_loop(i,d);

}
