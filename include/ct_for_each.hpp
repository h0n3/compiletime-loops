//
// Created by dvl on 9/24/19.
//

#ifndef H_COMPILETIME_LOOPS_CT_FOR_EACH_HPP
#define H_COMPILETIME_LOOPS_CT_FOR_EACH_HPP

#include <iostream>
#include <tuple>
#include <functional>

// clang-format off

template <class functor_t, class member_return_t, class... args_t>
void apply_member_func(functor_t& functor, member_return_t (functor_t::*func)(args_t...), std::tuple<args_t...>& data){
    std::apply(
        [&functor,func](args_t... args)
        {
            (functor.*func)(args...);
        },
        data
    );
}
template <class resolver_t, class data_t, class... functors_t>
void ct_for_each(data_t data, functors_t&... functors){
    (  apply_member_func(functors,resolver_t::resolve(functors), data)
    ,...);
}

template <class return_t, class first_t, class... args_t>
constexpr auto bind_first_func(first_t& first, return_t(*func)(first_t&, args_t...)){
    return [&first,func] (args_t... args){
        return func(first, args...);
    };
}

template <class resolver_t, class data_t, class... functors_t>
struct ct_for_each_tuple_impl{
    using this_t = ct_for_each_tuple_impl<resolver_t, data_t, functors_t...>;

    static inline void for_each(data_t& data, functors_t&... functors){
        ct_for_each<resolver_t, data_t, functors_t&...>(data, functors...);
    }

    static inline void eval(data_t& data, std::tuple<functors_t...>& functors){
    std::apply(
        bind_first_func(data, &this_t::for_each),
        functors
    );
}
};


template <class resolver_t, class data_t, class... functors_t>
void ct_for_each_tuple(data_t& data, std::tuple<functors_t...>& functors){
    ct_for_each_tuple_impl<resolver_t, data_t, functors_t...>::eval(data, functors);
}


template<class resolver_t, class... args_t>
struct call_helper{
    template<class... functors_t>
    static constexpr void call(std::tuple<args_t...>& data, std::tuple<functors_t...>& functors){
        ct_for_each_tuple<resolver_t, std::tuple<args_t...>, functors_t...>(data, functors);
    }
};


// clang-format on

#define DEFINE_FUNCTION(FUNCTION_NAME, ARGUMENT_TYPES...)                                           \
    struct FUNCTION_NAME##_resolver {                                                               \
        template <class functor_t>                                                                  \
        static constexpr auto resolve(functor_t& functor)                                           \
        {                                                                                           \
            return &functor_t::FUNCTION_NAME;                                                       \
        }                                                                                           \
    };                                                                                              \
    template <class... functors_t>                                                                  \
    static void call_##FUNCTION_NAME(                                                               \
        std::tuple<ARGUMENT_TYPES>& data, std::tuple<functors_t...>& functors)                      \
    {                                                                                               \
        call_helper<FUNCTION_NAME##_resolver, ARGUMENT_TYPES>::call<functors_t...>(data, functors); \
    }

#endif // H_COMPILETIME_LOOPS_CT_FOR_EACH_HPP
